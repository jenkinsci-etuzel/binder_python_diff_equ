from os import getenv, path
from pathlib import Path
import datetime
from dotenv import load_dotenv
import webdav3.client as wc

home = str(Path.home())

_EXPORT_FILE = "{}/export.csv".format(home)
_IMPORT_FILE = "{}/import.csv".format(home)

load_dotenv()

options = {
    'webdav_hostname': getenv("WEB_DAV_HOSTNAME"),
    'webdav_login': getenv("WEB_DAV_LOGIN"),
    'webdav_password': getenv("WEB_DAV_PASSWORD"),
    'webdav_disable_check': True
}


def download_import_file(file):
    client = wc.Client(options)
    client.download_file("webdav/{}/{}".format(getenv("SUPERSET_USERNAME"), file), _IMPORT_FILE)


def upload_export_file(remote_file):
    if path.isfile(_EXPORT_FILE):
        client = wc.Client(options)
        client.upload_sync(remote_path="webdav/{}/{}".format(getenv("SUPERSET_USERNAME"), remote_file), local_path=_EXPORT_FILE)


def list_user_files_in_webdav(user=None):
    if not user:
        user = getenv("SUPERSET_USERNAME")
    client = wc.Client(options)
    items = client.list(remote_path="webdav/{}".format(user), get_info=True)
    items.sort(key=lambda item: datetime.datetime.strptime(item["modified"], "%a, %d %b %Y %H:%M:%S %Z"))
    items = [item["name"].strip("/") for item in items if not item["isdir"] and not item["name"].startswith(".")]
    return items
